import React from 'react'
import IconMob from '../../img/app-ss-1.png'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'

const ExempleProject = () => {
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  }
  return (
    <section id="screenshots" className="screenshot-section pos-rel padding">
      <div className="right-pattern"></div>
      <div className="section-heading text-center mb-40">
        <h2>Наши работы</h2>
        <p>
          Данные проекты разработаны нашими усилиями. Проекты позволили
          <br />
          увеличить клиентооборот, сделали компанию узнаваемой и увеличили ее
          доход.
        </p>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-lg-10 offset-lg-1">
            <div
              id="screenshot-carousel"
              className="screenshot-carousel owl-carousel"
            >
              <Carousel responsive={responsive}>
                <div className="screenshot-item">
                  <img src={IconMob} alt="img" />
                </div>
                <div className="screenshot-item">
                  <img src={IconMob} alt="img" />
                </div>
                <div className="screenshot-item">
                  <img src={IconMob} alt="img" />
                </div>
                <div className="screenshot-item">
                  <img src={IconMob} alt="img" />
                </div>
              </Carousel>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ExempleProject
